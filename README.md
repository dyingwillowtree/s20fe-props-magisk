# Galaxy S20 FE 5G Props for Magisk

[![Based License](https://custom-icon-badges.herokuapp.com/badge/BASED_LICENSE-696969?logo=gigachad&style=for-the-badge)](https://github.com/thatonecalculator/BASED-LICENSE)

This is a Magisk module for setting your device's props to that of the Galaxy S20 FE 5G on GSI ROMs.
That's all it does.

Uploaded the source code here incase anybody wanted to make something similar.

Based on @T3SL4 on Telegram's work, who made the Pixel Props module.
